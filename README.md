# Results Van Schemas

Definitions of object structures, and associated validators, for data structures used by Results Van applications.

## JSON Schema

A JSON Schema is generated for files read or written by Results Van software.

You can get the latest version of schemas at `schemas.rst.vn`:

https://schema.rst.vn/timing-configuration.file.schema.json

Historical schemas are available in the GitLab package registry.

For instance, version 1.0.2 of the timing-configuration file is available at:

https://gitlab.com/api/v4/projects/resultsvan%2Fschemas/packages/generic/json-schema/1.0.2/timing-configuration.file.schema.json

## Node.js Library Usage

A Node.js library is provided to validate, read, and write
files that meet the defined schema.

The library ships with TypeScript definitions,
and works with TypeScript projects.
Other usage is not tested.

### Library Installation

#### NPM

```sh
npm config set @resultsvan:registry https://gitlab.com/api/v4/packages/npm/
npm install --save @resultsvan/schema
```

#### Yarn v1

```sh
npm config set @resultsvan:registry https://gitlab.com/api/v4/packages/npm/
yarn add @resultsvan/schema
```

#### Yarn Berry

Add the following to `.yarnrc.yml`

```yaml
npmScopes:
  resultsvan:
    npmRegistryServer: "https://gitlab.com/api/v4/packages/npm/"
```

Then run

```sh
yarn add @resultsvan/schema
```

### Read files using a schema

```ts
import fs from "fs";
import { transformAndValidateSync } from "class-transformer-validator";
import { TimingConfiguration } from "@resultsvan/schema/files";
// or:
// import { files } from "@resultsvan/schema";
// const { TimingConfiguration } = files;
import { BaseEvent } from "@resultsvan/schema/dto";

const file = fs.readFileSync("event.timing.json", "utf-8");
const config = transformAndValidateSync(TimingConfiguration, file);

// e is `Event extends BaseEvent`
const e = config.event;
```

### Validate Output Meets Schema

```ts
import fs from "fs";
import { validateOrReject } from "class-validator";
import { classToPlain } from "class-transformer";
import { TimingConfiguration } from "@resultsvan/schema/files";


async function writeConfig(
  config: TimingConfiguration,
) {
  await validateOrReject(config);
  const serialized = classToPlain(config);
  await fs.writeFileSync("event.timing.json", serialized);
}

writeConfig({
  event: {
    uuid: "...",
    id: "...",
    subEvents: [],
  },
});
```

## Dev Setup

### Setup dev.sh

You will need some private variables accessible on the shell.
We suggest creating a `.gitignore`-d file called `dev.sh` to store these secrets for quick loading.

```sh
# Create a PAT in GitLab with the 'api' scope
export GITLAB_NPM_TOKEN="..."
```

When you start a new shell, make sure you run `. dev.sh`.
