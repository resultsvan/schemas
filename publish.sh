version="$1"
AUTH_TOKEN="$CI_JOB_TOKEN"

upload_schema() {
  schema="$1"
  dest="$2"

  curl --header "JOB-TOKEN: $AUTH_TOKEN" --upload-file out/json-schema/$schema "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/json-schema/$version/$dest.schema.json"
}

upload_schema "timing-configuration-file.json" "timing-configuration.file"
