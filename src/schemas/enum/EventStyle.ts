/**
 * The general style/competitiveness of the event.
 */
export enum EventStyle {

  /**
   * For general non-competitive events, e.g. fun rides.
   */
  NON_COMPETITIVE = "non-competitive",

  /**
   * Generally non/minimally competitive events for training.
   */
  TRAINING = "training",

  /**
   * Generally non/minimally competitive events for routine training.
   *
   * Generally a more-exclusive/club-only group of participants
   * compared to {@link TRAINING} events.
   */
  PRACTICE = "practice",

  /**
   * A no-stakes competitive event used for practice or training.
   */
  PRACTICE_COMPETITION = "practice-competition",

  /**
   * A regular competitive race or other event.
   */
  COMPETITION = "competition",

  /**
   * An event focused on charity, even if competitive.
   */
  CHARITY_EVENT = "charity",

  /**
   * An event that does not fall into any of the established categories.
   */
  OTHER = "other",

}
