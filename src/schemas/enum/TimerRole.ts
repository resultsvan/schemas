export enum TimerRole {

  /**
   * A device that submits data associated with a (potentially) relevant moment,
   * including rider presence, an important time, or some other event.
   *
   * Creates `TimeEvent` events linked to each capture.
   */
  EVENT_CAPTURE = "event",

  /**
   * A device that captures data in response to another event,
   * such as a still-picture camera that takes photos once a laser detects rider presence.
   *
   * Receives `TimeEvent` signals and produces associated `TimeEvent`s linked to the source.
   */
  TRIGGERED_CAPTURE = "triggered",

  /**
   * A stream of data that is captured unassociated with specific events,
   * but is vital for timing, e.g. line-scan photos.
   */
  TIMING_STREAM = "timing_stream",

  /**
   * A stream of data that is captured unassociated with specific events,
   * and is not vital for timing, e.g. a secondary video stream that may help for rider identification.
   */
  STREAM_CAPTURE = "stream",

}
