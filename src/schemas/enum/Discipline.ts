/**
 * General discipline of sport.
 */
export enum Discipline {

  ROAD_BIKING = "road",

  MOUNTAIN_BIKING = "mtb",

  CYCLOCROSS = "cx",

  OTHER = "other",

}
