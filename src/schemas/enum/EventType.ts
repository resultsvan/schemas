export enum EventType {

  ROAD_RACE = "road-race",
  ROAD_CRIT = "crit",
  ROAD_CIRCUIT = "road-circuit",
  ROAD_TIME_TRIAL = "road-itt",
  ROAD_TEAM_TT = "road-ttt",

  MTB_CROSS_COUNTRY = "mtb-xc",
  MTB_SHORT_TRACK = "mtb-stxc",
  MTB_DUEL_SLALOM = "mtb-ds",
  MTB_TEAM_RELAY = "mtb-tr",
  MTB_ENDURO = "mtb-enduro",
  MTB_DOWNHILL = "mtb-dh",
  MTB_SUPER_DOWNHILL = "mtb-super-downhill",

  CYCLOCROSS_RACE = "cx",
  CYCLOCROSS_RELAY = "cx-tr",

  GRAVEL_RACE = "gravel",

  OTHER = "other",

}
