import { Type } from "class-transformer";
import {
  IsBoolean,
  IsDate,
  IsNumber,
  IsOptional,
  Max,
  Min,
  ValidateIf,
  ValidateNested,
} from "class-validator";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";

export class FieldStart {

  @IsBoolean()
  isToFollow: boolean;

  /**
   * Number of seconds to this field is delayed from when the wave starts.
   *
   * Must be within 1 week of the wave starting.
   */
  @ValidateIf((start: FieldStart) => !start.isToFollow)
  @IsNumber()
  @Min(0)
  @Max(60 * 60 * 168)
  startOffset?: number;

  @ValidateIf((start: FieldStart) => start.isToFollow)
  @IsNanoID(NanoDictionary.DEFAULT, 12)
  follows?: NanoID;

}

/**
 * A field of participants in a wave for an event that start together.
 *
 * Generally allowed to mingle/draft/otherwise interact.
 */
export class BaseField {

  @IsNanoID(NanoDictionary.DEFAULT, 12)
  id: NanoID;

  @IsOptional()
  @ValidateNested()
  @Type(() => FieldStart)
  start?: FieldStart;

}
