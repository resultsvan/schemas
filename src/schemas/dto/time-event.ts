import { Equals, IsBoolean, IsDate, IsEnum, IsNumber, IsOptional, IsString, IsUUID } from "class-validator";
import { TimeEventType } from "../enum/TimeEventType";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";

export class BaseTimeEvent {
  /**
   * A unique ID for this event.
   *
   * Highly collision resistant in the context of an event.
   * High collision chance if used between events.
   */
  @IsNanoID(NanoDictionary.ALPHANUMERIC, 12)
  id: NanoID;

  /**
   * The date of this event.
   * Copy the millisecond time (with decimals for sub-millisecond times) to {@link ms},
   * and use {@link d} to represent how accurate this measurement is.
   */
  @IsDate()
  date: Date;

  /**
   * A duplicate of the number of milliseconds in the time,
   * with the ability to use decimals for sub-millisecond timing.
   */
  @IsNumber({
    maxDecimalPlaces: 5,
  })
  ms: number;

  /**
   * The accuracy, represented as the maximum number of milliseconds
   * the number is likely off. (`d` = maximum delta)
   *
   * For instance, a time that is possibly 3 seconds off would have a `ds`
   * of `3000`.
   */
  @IsNumber()
  d: number;

  /**
   * The author of this event - either a {@link EventTimerConfig.id} for events
   * created by timing devices, or {@link TimingManagerNodeDefinition.id}
   * for manually authored events.
   */
  @IsNanoID(NanoDictionary.SAFE, 4)
  by: NanoID;

  /**
   * If a person has validated this as a true event.
   */
  @IsOptional()
  @IsBoolean()
  good?: boolean;

  /**
   * If {@link good}, the staff member who confirmed this event.
   */
  @IsOptional()
  @IsUUID()
  confirmedBy?: string;

  /**
   * If this event has been dismissed (e.g. an invalid/irrelevant event),
   * or if it has been replaced by a more accurate or detailed event.
   */
  @IsOptional()
  @IsBoolean()
  old?: boolean;

  /**
   * A list of older {@link TimeEvent} nodes this event replaces.
   */
  @IsOptional()
  @IsString({
    each: true,
  })
  replaces?: string[];

  /**
   * An optional comment that might be added to a note.
   *
   * May have a specific use based on type, for instance as a way to note additional
   * rider identification if a bib wasn't legible.
   */
  @IsOptional()
  @IsString()
  comment?: string;
}

export class ParticipantLapTimeEvent extends BaseTimeEvent {
  @Equals(TimeEventType.PARTICIPANT_LAP)
  type: TimeEventType.PARTICIPANT_LAP;

  @IsOptional()
  @IsString()
  bib?: string;
}

export class ParticipantFinishTimeEvent extends BaseTimeEvent {
  @Equals(TimeEventType.PARTICIPANT_FINISH)
  type: TimeEventType.PARTICIPANT_FINISH;

  @IsOptional()
  @IsString()
  bib?: string;
}

export class AnyTimeEvent extends BaseTimeEvent {

  @IsEnum(TimeEventType)
  type: TimeEventType;

  @IsOptional()
  @IsString()
  bib?: string;

}

export type TimeEvent = ParticipantLapTimeEvent | ParticipantFinishTimeEvent;
