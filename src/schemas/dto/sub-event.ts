import { Type } from "class-transformer";
import {
  IsBoolean,
  IsDate,
  IsEnum,
  IsOptional,
  IsString,
  ValidateIf,
  ValidateNested,
} from "class-validator";
import { Discipline } from "../enum/Discipline";
import { EventStyle } from "../enum/EventStyle";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";

export class SubEventStart {

  @IsBoolean()
  isToFollow: boolean;

  @ValidateIf((start: SubEventStart) => !start.isToFollow)
  @IsDate()
  startTime?: Date;

  @ValidateIf((start: SubEventStart) => start.isToFollow)
  follows?: NanoID;

}

/**
 * A specific race/event that is part of a larger {@link BaseEvent}.
 *
 * For instance, a stage race {@link Event} may have sub-events
 * for each day, one for a road race and one for a crit.
 */
export class BaseSubEvent {

  /**
   * The ID for this event.
   */
  @IsNanoID(NanoDictionary.DEFAULT, 8)
  id: NanoID;

  /**
   * The full name of the event.
   */
  @IsOptional()
  @IsString()
  name: string;

  /**
   * When this event starts.
   */
  @ValidateNested()
  @Type(() => SubEventStart)
  start: SubEventStart;

  /**
   * The overall sport/discipline for the event.
   */
  @IsEnum(Discipline)
  discipline: Discipline;

  /**
   * The general style/competitiveness of the event.
   * Not every wave/field may match this style, for instance
   * a {@link EventStyle.COMPETITION} race may have a field that is for new riders to practice.
   */
  @IsEnum(EventStyle)
  defaultStyle: EventStyle;

}
