import { IsOptional, IsString, ValidateNested } from "class-validator";
import { EventTimingDevice } from "./event-timing-device";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";
import { Type } from "class-transformer";

/**
 * Describes a management node used to time an event.
 *
 * Each management node runs independently, with consistent
 * access to its associated cameras and other timing equipment.
 *
 * Staff timing an event will specify which manager node they will be
 * consistently accessing inside the UI, and their data will be stored with the node.
 *
 * One node will be designated 'primary', and will merge the streams from any secondary
 * nodes into the final race report.
 */
export class TimingManagerNodeDefinition {

  /**
   * A unique ID to distinguish this manager from other nodes in the context of this event.
   */
  @IsNanoID(NanoDictionary.SAFE, 4)
  id: NanoID;

  /**
   * A human-readable description of this node -
   * "finish line", "start line", "left side" could be useful descriptions.
   */
  @IsOptional()
  @IsString()
  name?: string;

  /**
   * Timing devices that will be collated by this node.
   */
  @ValidateNested()
  @Type(() => EventTimingDevice)
  timingDevices: EventTimingDevice[];

}
