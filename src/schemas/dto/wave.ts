import { Type } from "class-transformer";
import {
  IsBoolean,
  IsDate,
  IsEnum,
  IsOptional,
  IsString,
  ValidateIf,
  ValidateNested,
} from "class-validator";
import { EventType } from "../enum/EventType";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";

export class WaveStart {

  /**
   * If this wave follows after a prior wave ends,
   * or if it is scheduled to start at a time.
   */
  @IsBoolean()
  isToFollow: boolean;

  /**
   * If not {@link isToFollow}, the time this wave starts.
   */
  @ValidateIf(start => !start.isToFollow)
  @IsDate()
  startTime?: Date;

  /**
   * If {@link isToFollow}, the ID of the {@link BaseWave} to follow.
   */
  @ValidateIf(start => start.isToFollow)
  @IsNanoID(NanoDictionary.SAFE, 10)
  waveId?: NanoID;

}

/**
 * A wave of an event, where participants start at the same time
 * (or sequentially after other fields).
 */
export class BaseWave {

  @IsNanoID(NanoDictionary.SAFE, 10)
  id: NanoID;

  @IsOptional()
  @IsString()
  name?: string;

  /**
   * The start of the wave.
   */
  @IsOptional()
  @ValidateNested()
  @Type(() => WaveStart)
  start?: WaveStart;

  /**
   * The type of event.
   */
  @IsOptional()
  @IsEnum(EventType)
  eventType?: EventType;
}
