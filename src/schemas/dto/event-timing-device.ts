import { Type } from "class-transformer";
import { IsOptional, IsString, ValidateNested } from "class-validator";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";
import { EventTimerConfig } from "./event-timer-config";

/**
 * A device that is intended to be used during an event,
 * including cameras, lasers, and other devices.
 */
export class EventTimingDevice {

  /**
   * A unique ID for this device.
   */
  @IsNanoID(NanoDictionary.SAFE, 4)
  id: NanoID;

  /**
   * A human-readable name to identify this device.
   */
  @IsOptional()
  @IsString()
  name?: string;

  /**
   * Streams/capture types of data.
   */
  @ValidateNested()
  @Type(() => EventTimerConfig)
  data: EventTimerConfig[];

}
