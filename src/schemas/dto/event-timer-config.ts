import { IsEnum, IsOptional, IsString } from "class-validator";
import { TimerRole } from "../enum/TimerRole";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";

/**
 * Configuration for {@link EventTimerConfig} data capture/streams
 * that controls how captured events are stored.
 */
export class EventTimerEventsConfig {

  /**
   * Set to `false` if the data is noisy with false positives,
   * e.g. a laser that may have spectators walking in front of,
   * or a video camera determining rider presence that has false positives.
   */
  noFalsePositives: boolean;

}

/**
 * A specific stream or capture of data returned by a timing device.
 * Each device may have more than one config if they have multiple outputs;
 * for instance a camera may produce both a video stream and specific captured photos.
 */
export class EventTimerConfig {

  /**
   * An ID for this data stream that should be unique for an entire installation.
   * Used for file and database names.
   *
   * Use {@link name} or {@link id} to display to the user.
   */
  @IsNanoID(NanoDictionary.SAFE, 8)
  uuid: NanoID;

  /**
   * A unique ID to distinguish this data stream/capture for the event.
   * Use for display/in-event data, use {@link uuid} for file/database names.
   */
  @IsNanoID(NanoDictionary.SAFE, 4)
  id: NanoID;

  /**
   * A human-readable name for this stream.
   */
  @IsOptional()
  @IsString()
  name?: string;

  /**
   * The general role of this timing data.
   */
  @IsEnum(TimerRole)
  role: TimerRole;

  /**
   * Configuration relevant for {@link TimerRole.EVENT_CAPTURE} timers,
   * controlling how captured events are interpreted.
   */
  event?: EventTimerEventsConfig;

}
