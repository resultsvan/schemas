import { IsOptional, IsString, IsUUID } from "class-validator";
import { IsNanoID, NanoDictionary } from "../scalar/nanoid";

/**
 * An abstract event that can contain one or more {@link SubEvent}s,
 * generally all occurring sequentially/simultaneously, with similar groups of people.
 */
export class BaseEvent {

  /**
   * A globally unique ID for this event.
   * Generated IDs should be globally unique (even between other software installations),
   * but conflicts may occur, human created or otherwise.
   */
  @IsUUID()
  uuid: string;

  /**
   * A very short human-readable ID.  This should be displayed in almost every case.
   *
   * Limited chance of conflict, but {@link uuid} should be used instead internally.
   */
  @IsNanoID(NanoDictionary.DEFAULT, 6)
  id: string;

  /**
   * A user-set name that can be used for filenames and such.
   */
  @IsOptional()
  @IsString()
  customId?: string;

  /**
   * The name of the event.
   */
  @IsOptional()
  @IsString()
  name?: string;

}
