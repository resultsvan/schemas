import { Type } from "class-transformer";
import { IsOptional, ValidateNested } from "class-validator";
import { BaseEvent } from "../dto/event";
import { BaseField } from "../dto/field";
import { TimingManagerNodeDefinition } from "../dto/timing-manager-node";
import { BaseSubEvent } from "../dto/sub-event";
import { BaseWave } from "../dto/wave";
import { IsNanoID, NanoDictionary, NanoID } from "../scalar/nanoid";

class Field extends BaseField {

}

class Wave extends BaseWave {

  @ValidateNested({ each: true })
  @Type(() => Field)
  fields: Field[];

}

/**
 * Each individual event that will be timed.
 */
class SubEvent extends BaseSubEvent {

  /**
   * Each starting wave of the event.
   */
  @ValidateNested({ each: true })
  @Type(() => Wave)
  waves: Wave[];

}

/**
 * The overall event that will be timed.
 */
export class Event extends BaseEvent {

  /**
   * Each race/ride/event that is part of this overall event.
   *
   * Many events will have a single sub-event, for instance
   * most cyclocross events will have a single sub-event for each day.
   */
  @ValidateNested({ each: true })
  @Type(() => SubEvent)
  subEvents: SubEvent[];

}

export class TimingConfigurationFile {

  /**
   * The overall event that will be timed (in full or in part)
   */
  @ValidateNested()
  @Type(() => Event)
  event: Event;

  /**
   * The IDs of sub-events ({@link SubEvent.id}) that should be timed.
   * If not provided, then all listed events can be timed.
   *
   * If empty array, system will assume it should not time any events.
   */
  @IsOptional()
  @IsNanoID(NanoDictionary.DEFAULT, 8, {
    each: true,
  })
  timedSubEvents?: NanoID[];

  /**
   * Which of the manager nodes will be the primary
   * that can generate the race report.
   */
  @IsNanoID(NanoDictionary.SAFE, 4)
  primaryManagerId: NanoID;

  /**
   * The manager nodes that will time the event.
   */
  @ValidateNested()
  @Type(() => TimingManagerNodeDefinition)
  managerNodes: TimingManagerNodeDefinition[];

}
