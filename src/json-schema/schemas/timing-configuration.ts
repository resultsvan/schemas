import { writeFileSync } from "fs";
import { validationMetadatasToSchemas } from "class-validator-jsonschema";
import { defaultMetadataStorage } from "class-transformer/cjs/storage";
import "../../schemas/files/timing-configuration";
import path from "path";

const schemas = validationMetadatasToSchemas({
  classTransformerMetadataStorage: defaultMetadataStorage,
});
writeFileSync(path.join(__dirname, "../../../out/json-schema/timing-configuration-file.json"), JSON.stringify(schemas, undefined, 2));
