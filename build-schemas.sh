#!/bin/sh

build() {
  schema="$1"
  node "out/json-schema/schemas/$schema.js"
}

build "timing-configuration"
