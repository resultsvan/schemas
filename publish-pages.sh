publish_schema() {
  schema="$1"
  dest="$2"

  cp out/json-schema/$schema public/$dest.schema.json
}

mkdir -p public/

publish_schema "timing-configuration-file.json" "timing-configuration.file"
